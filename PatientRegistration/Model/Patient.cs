﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientRegistration.Model
{
    public class Patient
    {
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SecondName { get; set; }
        public byte Sex { get; set; }
        public DateTime BirthDate { get; set; }
        public virtual List<Visit> Visits { get; set; }
    }
}
