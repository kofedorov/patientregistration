﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientRegistration.Model
{
    public class Visit
    {
        public int VisitId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
