﻿using PatientRegistration.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PatientRegistration.Views
{
    /// <summary>
    /// Interaction logic for PatientsListControl.xaml
    /// </summary>
    public partial class PatientsList : UserControl
    {
        public PatientsList()
        {
            InitializeComponent();

            //DataContext = (ViewModel.MainViewModel)this.DataContext;

            ViewModel.MainViewModel vm = (ViewModel.MainViewModel)this.DataContext;

            if(vm != null) MessageBox.Show(vm.PatientList[0].LastName);
        }

        private void btnAddPatient_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnOpenPatient_Click(object sender, RoutedEventArgs e)
        {

        }

        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
