﻿using PatientRegistration.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientRegistration
{
    class PatientContext: DbContext
    {
        static PatientContext()
        {
            Database.SetInitializer<PatientContext>(new PatientContextInit());
        }

        public PatientContext() : base("DbConnection")
        { }


        public DbSet<Patient> Patients { get; set; }
    }

    class PatientContextInit : DropCreateDatabaseAlways<PatientContext>
    {
        protected override void Seed(PatientContext context)
        {
            context.Patients.Add(new Patient { PatientId = 1, FirstName = "Иван", LastName = "Иванов", SecondName = "Иванович", Sex = 1, BirthDate = new DateTime(1987, 12, 8, 0, 0, 0, 0) });
            context.Patients.Add(new Patient { PatientId = 2, FirstName = "Петр", LastName = "Петров", SecondName = "Петрович", Sex = 1, BirthDate = new DateTime(1988, 5, 1, 0, 0, 0, 0) });
            context.Patients.Add(new Patient { PatientId = 3, FirstName = "Сидор", LastName = "Сидоров", SecondName = "Сидорович", Sex = 1, BirthDate = new DateTime(1989, 10, 25, 0, 0, 0, 0) });

            context.SaveChanges();
        }
    }
}
