﻿using PatientRegistration.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PatientRegistration.ViewModel
{
    class MainViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        #endregion

        #region Command

        public IAsyncCommand TestCommand { get; private set; }

        #endregion

        #region Property

        private string _testProperty = String.Empty;
        public string TestProperty
        {
            get => _testProperty;
            set
            {
                _testProperty = value;
                OnPropertyChanged("TestProperty");
            }
        }

        private ObservableCollection<Patient> _patientList { get; set; }
        public ObservableCollection<Patient> PatientList
        {
            get { return _patientList; }
            set
            {
                if (_patientList != value)
                {
                    _patientList = value;
                    OnPropertyChanged("PatientList");
                }
            }
        }

        #endregion

        public MainWindow mainWindow;

        public MainViewModel()
        {
            MainWindow window = new MainWindow();
            window.DataContext = this;
            mainWindow = window;

            PatientList = new ObservableCollection<Patient>();

            TestCommand = new AsyncCommand(async () =>
            {
            });

            //mainWindow.DataContext = this;
        }

        public void ShowMainWindow()
        {
            ShowNeededControl("PatientsList");
        }

        private void ShowNeededControl(string nameControlToShow)
        {
            foreach (System.Windows.UIElement uie in mainWindow.GridControl.Children)
            {
                if (string.Compare(uie.DependencyObjectType.Name, nameControlToShow) == 0)
                    uie.Visibility = System.Windows.Visibility.Visible;
                else
                    uie.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
