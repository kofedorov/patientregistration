﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace PatientRegistration
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            ViewModel.MainViewModel VM = new ViewModel.MainViewModel();
            try
            {
                using (PatientContext db = new PatientContext())
                {
                    foreach (var patient in db.Patients)
                    {
                        //System.Diagnostics.Debug.WriteLine($"id: {patient.PatientId} first name: {patient.FirstName} second name: {patient.LastName} last name: {patient.SecondName}");
                        VM.PatientList.Add(patient);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(PatientRegistration.Properties.Resources.OopsSomethingWentWrong, "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            VM.ShowMainWindow();
        }
    }
}
