﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PatientRegistration.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    /// <summary>
    /// A circular type progress bar, that is simliar to popular web based
    /// progress bar.
    /// </summary>
    /// <remarks>
    /// The circles in the CircularProgressBar use the Foreground brush of the 
    /// CircularProgressBar as their fill.
    /// </remarks>
    /// <example>
    /// <![CDATA[<CircularProgressBar Width="80" Height="80" Foreground="Red" />]]>
    /// </example>
    /// <history>
    /// 06.05.2010 - Draft (MicMen)
    /// </history>
    public partial class CircularProgressBar : UserControl
    {
        #region Private data

        private readonly DispatcherTimer _animationTimer;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CircularProgressBar"/> class.
        /// </summary>
        /// <history>
        /// 06.05.2010 - draft (MicMen)
        /// </history>
        public CircularProgressBar()
        {
            InitializeComponent();

            _animationTimer = new DispatcherTimer(DispatcherPriority.ContextIdle, Dispatcher);
            _animationTimer.Interval = new TimeSpan(0, 0, 0, 0, 75);
        }

        #endregion

        /// <summary>
        /// Starts the animation.
        /// </summary>
        /// <history>
        /// 06.05.2010 - Draft (MicMen)
        /// </history>
        private void StartAnimation()
        {
            _animationTimer.Tick += OnAnimationTick;
            _animationTimer.Start();
        }

        /// <summary>
        /// Stops the animation.
        /// </summary>
        /// <history>
        /// 06.05.2010 - Draft (MicMen)
        /// </history>
        private void StopAnimation()
        {
            _animationTimer.Stop();
            _animationTimer.Tick -= OnAnimationTick;
        }

        /// <summary>
        /// Handles the animation tick.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// <history>
        /// 06.05.2010 - Draft (MicMen)
        /// </history>
        private void OnAnimationTick(object sender, EventArgs e)
        {
            SpinnerRotate.Angle = (SpinnerRotate.Angle + 36) % 360;
        }

        /// <summary>
        /// Handles the loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        /// <history>
        /// 06.05.2010 - Draft (MicMen)
        /// </history>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            const double offset = Math.PI;
            const double step = Math.PI * 2 / 10.0;

            SetPosition(C0, offset, 0.0, step);
            SetPosition(C1, offset, 1.0, step);
            SetPosition(C2, offset, 2.0, step);
            SetPosition(C3, offset, 3.0, step);
            SetPosition(C4, offset, 4.0, step);
            SetPosition(C5, offset, 5.0, step);
            SetPosition(C6, offset, 6.0, step);
            SetPosition(C7, offset, 7.0, step);
            SetPosition(C8, offset, 8.0, step);
        }

        /// <summary>
        /// Sets the position of the ellipse.
        /// </summary>
        /// <param name="ellipse">The ellipse.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="posOffSet">The pos off set.</param>
        /// <param name="step">The step.</param>
        /// <history>
        /// 06.05.2010 - draft (MicMen)
        /// </history>
        private void SetPosition(Ellipse ellipse, double offset, double posOffSet, double step)
        {
            ellipse.SetValue(Canvas.LeftProperty, 50.0 + Math.Sin(offset + posOffSet * step) * 50.0);
            ellipse.SetValue(Canvas.TopProperty, 50 + Math.Cos(offset + posOffSet * step) * 50.0);
        }

        /// <summary>
        /// Handles the unloaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        /// <history>
        /// 06.05.2010 - draft (MicMen)
        /// </history>
        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            StopAnimation();
        }

        /// <summary>
        /// Handles the visible changed.
        /// </summary>
        /// <remarks>
        /// This method is called when the Visibility of the control changes. When the control is collapsed
        /// or hidden the animation will be stoped to save CPU time.
        /// </remarks>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        /// <history>
        /// 06.05.2010 - draft (MicMen)
        /// </history>
        private void OnVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            bool isVisible = (bool)e.NewValue;

            if (isVisible)
            {
                StartAnimation();
            }
            else
            {
                StopAnimation();
            }
        }
    }
}
